# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 13:52:32 2022

@author: Stefan
"""
class Person:
    def __init__(self, name='Other', last_name='Person', birth_year=None):
        self.name = name
        self.last_name = last_name
        self.full_name = None
        self.birth_year = birth_year
    
    def get_full_name(self):
        self.full_name = self.name + " " + self.last_name
        return self.full_name
    
    def calculate_age(self):
        age = 2022 - self.birth_year
        return age

class Student(Person):
    def __init__(self, name, last_name):
        workshop = None
            
    def enroll(self, workshop):
        self.workshop = workshop


if __name__ == '__main__':   
    you = Person('John', 'Wayne')
    print(you.name)
    print(you.last_name)

    someone = Person()
    print(someone.name)
    print(someone.last_name)