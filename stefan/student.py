# -*- coding: utf-8 -*-
"""
Created on Thu Jul 21 13:52:32 2022

@author: Stefan
"""
from person import Person

class Student(Person):
    def __init__(self, name, last_name):
        super().__init__(name, last_name)
        workshop = None
            
    def enroll(self, workshop):
        self.workshop = workshop
        
    def __str__(self): # DUNDER
        return f"Student {self.name} {self.last_name}"

if __name__ == '__main__':    
    me = Student('Stefan', 'Werzinger')
    print(me.name)
    print(me.last_name)
    me.enroll('Python for the Lab')
    print(me.workshop)